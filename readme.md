<p style= "text-align:center;">Created by theangrygrunt</p>  
Linux utility that changes the case of inputted strings and pastes them into the system clipboard via xclip  

Compilation Instructions (**linux only**):  
Compile with gcc: gcc changecase.c -o ccase

Required dependancies/programs  
-xclip

--Known issues--  
Executing without launch arguments will cause a segfault instead of displaying an error message

--VERSION HISTORY--  
May 3, 2024  
Internal 1.0 version with 240 charicter limit

June 18, 2024  
Version 2.0 (initial git version):  
-Launch arguments  
-Logging support  
-Increase charicter limit to 1000  
-filesize optomizations  