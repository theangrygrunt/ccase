//libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define MAXVAL 1001 // eventually it will be dynamically allocated 

//prototypes
void proccessText(char string[MAXVAL], int position);
void pipeToClipboard(char text[MAXVAL], char **argv);
void transcribetofile (char input[MAXVAL], FILE* file);
bool checkarguments (int argc, char **argv);

//code
int main (int argc, char* argv[]){
  if (checkarguments(argc, argv)==false){ 
    exit(0);
  }
  int counter = 0;
  char text[MAXVAL];
  
  // if statement to determine if *argv[2] has any text inside of it
  if (argv[2] == NULL){
    printf("Input text (1000 charicters max): "); //change charicter limit to 1,000 charicters, maybe more
    fgets(text, MAXVAL ,stdin); // stdin points to the standard input
  }
  else {
    // transcribe charicters to the text array
    for(int count = 0; argv[2][count]!=00; count++){
      text[count] = argv[2][count];
    } 
  }
  while (counter < strlen(text)){
    proccessText(text, counter);
    counter++;
  }
  // output
  printf("Output: %sString length: %ld\n ", text, strlen(text));//%ld= long int
  pipeToClipboard(text, argv);
  return 0;
}

void proccessText(char string[MAXVAL], int position){
  int asciiValue = string[position]; // buffer that gets reset per charicter
  if (asciiValue>= 97 && asciiValue <= 122){ // lowercase charichters
    // subtract ASCII value by 32
    string[position] = string[position] - 32;
  }
  if (asciiValue >= 65 && asciiValue <= 90){ // uppercase charicters
    // add ASCII value by 32
    string[position] = string[position] + 32;
  }
} 

void pipeToClipboard (char text2[MAXVAL], char **argv){
  FILE* holdingfile;
  holdingfile = fopen("holding.txt","a+");
  transcribetofile(text2, holdingfile);
  fclose(holdingfile);
  system("xclip -selection clipboard holding.txt");
  remove("holding.txt"); // internal pipe is already closed, using actual filename is required
  printf("Contents pasted to system clipboard\n");
  if (argv[1][1] == 107){ // if logging flag is detected
    FILE* logfile;
    logfile = fopen("log.txt","a+");
    transcribetofile(text2, logfile);
    printf("Arguments used, %s",argv[1]);
  }
}

void transcribetofile (char input[MAXVAL], FILE* file){
  for (int counter=0; counter < strlen(input); counter++){
    fprintf(file,"%c",input[counter]);
  }
}

bool checkarguments(int argc, char **argv){
  // When inserting new arguments, please refer an ASCII table. The argument checker does NOT support charicters or strings.
  switch(argv[1][1]){
    case 104:
      system("cat help.dat");
      return(false); //even though -h is a valid argument the program still needs to be stopped 
      break;
    case 100:
      system("cat log.txt");
      return(false);
    case 107:
    case 110:
      return(true);
      break;
    case 0: //null
      printf("No arguments, use the -h argument to see a list of supported commands.\nProgram exiting...\n");
      return(false);
      break;
    default:
      printf("Argument not supported, use the -h argument to list avalible commands.\nProgram exiting...\b");
      return(false);
      break;
  }
} 